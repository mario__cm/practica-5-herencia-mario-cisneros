package clases;
/**
 * 
 * @author Mario
 *
 */
public class Coches {

	private String matricula;
	private String modelo;
	
	
	/**
	 * objeto coche.
	 * @param matricula
	 */
	public Coches(String matricula) {
		this.matricula = matricula;
	}
	
	
	@Override
	public String toString() {
		return "Coches [matricula = " + matricula + ", modelo = " + modelo + "]";
	}


	public String getMatricula() {
		return matricula;
	}
	public void setMatricula(String matricula) {
		this.matricula = matricula;
	}
	public String getModelo() {
		return modelo;
	}
	public void setModelo(String modelo) {
		this.modelo = modelo;
	}
	
	
}
