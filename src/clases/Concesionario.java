package clases;
/**
 * 
 * @author Mario
 *
 */
public class Concesionario {

	protected String nombre;
	protected String direccion;
	protected int cantidadVehiculos;
	protected int numeroTrabajadores;
	protected String numeroTelefono;
	protected Marcas [] marcas;
	

/**
 * atributos de concesionario.
 * @param nombre
 * @param direccion
 * @param cantidadVehiculos
 * @param numeroTrabajadores
 * @param maxMarcas
 * @param numeroTelefono
 */
	//Constructor con parametros
	public Concesionario(String nombre, String direccion, int cantidadVehiculos, int numeroTrabajadores, int maxMarcas,
			String numeroTelefono) {
		super();
		this.nombre = nombre;
		this.direccion = direccion;
		this.cantidadVehiculos = cantidadVehiculos;
		this.numeroTrabajadores = numeroTrabajadores;
		this.numeroTelefono = numeroTelefono;
		this.marcas = new Marcas [maxMarcas];
		
	}
	
	
	//Constructor sin parametros
	public Concesionario() {
		this.nombre = "";
		this.direccion = "";
		this.cantidadVehiculos = 0;
		this.numeroTrabajadores = 0;
		this.numeroTelefono = "";
		this.marcas = new Marcas [0];
	
	}
/**
 * atributos nueva marca
 * @param codigo
 * @param nombre
 */
	public void nuevaMarca(String codigo, String nombre) {
		for (int i = 0; i < marcas.length; i++) {
			if (marcas[i] == null) {
				marcas[i] = new Marcas(codigo);
				marcas[i].setNombre(nombre);
				break;
			}
		}
	}
	/**
	 * lista las marcas
	 */
	public void listarMarcas() {

		for (int i = 0; i < marcas.length; i++) {
			if (marcas[i] == null) {
				System.out.println(marcas[i]);
			}
		}
	}
/**
 * array de marcas
 * @return
 */
	public Marcas[] getMarcas() {
		return marcas;
	}


	public void setMarcas(Marcas[] marcas) {
		this.marcas = marcas;
	}

	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getDireccion() {
		return direccion;
	}
	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}
	public int getCantidadVehiculos() {
		return cantidadVehiculos;
	}
	public void setCantidadVehiculos(int cantidadVehiculos) {
		this.cantidadVehiculos = cantidadVehiculos;
	}
	public int getNumeroTrabajadores() {
		return numeroTrabajadores;
	}
	public void setNumeroTrabajadores(int numeroTrabajadores) {
		this.numeroTrabajadores = numeroTrabajadores;
	}
	public String getNumeroTelefono() {
		return numeroTelefono;
	}
	public void setNumeroTelefono(String numeroTelefono) {
		this.numeroTelefono = numeroTelefono;
	}



	@Override
	public String toString() {
		return "\n\nConcesionario:\nNombre = " + nombre + "\nDireccion = " + direccion + "\nCantidadVehiculos = "
				+ cantidadVehiculos + "\nNumeroTrabajadores = " + numeroTrabajadores + "\nNumeroTelefono = "
				+ numeroTelefono + "\n\n";
	}
	
	
	
	
}
