package clases;
/**
 * 
 * @author Mario
 *
 */
public class Motos {

	private String matricula;
	private String modelo;
	private String carenado;
	/**
	 * objeto motos
	 * @param matricula
	 */
	public Motos(String matricula) {
		this.matricula = matricula;
	}
	
	public String getMatricula() {
		return matricula;
	}
	public void setMatricula(String matricula) {
		this.matricula = matricula;
	}
	public String getModelo() {
		return modelo;
	}
	public void setModelo(String modelo) {
		this.modelo = modelo;
	}


	public String getCarenado() {
		return carenado;
	}

	public void setCarenado(String carenado) {
		this.carenado = carenado;
	}

	@Override
	public String toString() {
		return "Motos [ matricula = " + matricula + ", modelo = "  + modelo + "]";
	}
	
	
	
}
