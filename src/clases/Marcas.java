package clases;

/**
 * 
 * @author Mario
 *
 */

public class Marcas {

	private String codigo;
	private String nombre;
	
/**
 * objeto marcas	
 * @param codigo
 */
	public Marcas(String codigo) {
		this.codigo = codigo;
	}
	
	public String toString() {
		return "Marcas [nombre =" + nombre + ", codigo =" + codigo + "]";
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}
	
}
