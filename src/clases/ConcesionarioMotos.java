package clases;

import java.util.Arrays;
/**
 * 
 * @author Mario
 *
 */
public class ConcesionarioMotos extends Concesionario {


	private Motos[] motos;
	private String equipamiento;

	
	/**
	 * atributos concesonario motos
	 * @param nombre
	 * @param direccion
	 * @param cantidadVehiculos
	 * @param numeroTrabajadores
	 * @param maxMarcas
	 * @param numeroTelefono
	 * @param maxMotos
	 * @param equipamiento
	 */
	public ConcesionarioMotos(String nombre, String direccion, int cantidadVehiculos, int numeroTrabajadores,
			int maxMarcas, String numeroTelefono, int maxMotos, String equipamiento) {
		super(nombre, direccion, cantidadVehiculos, numeroTrabajadores, maxMarcas, numeroTelefono);
		this.motos = new Motos [maxMotos];
		this.equipamiento = equipamiento;
		this.nombre = "MC MOTOS";
	}
	
	
	public ConcesionarioMotos() {
		super();
		this.motos = new Motos [0];
		this.equipamiento = "";
	}
	
	
	/**
	 * atributos y dar de alta moto
	 * @param matricula
	 * @param modelo
	 * @param carenado
	 */
	public void altaMoto(String matricula, String modelo, String carenado) {
		for (int i = 0; i < motos.length; i++) {
			if (motos[i] == null) {
				motos[i] = new Motos(matricula);
				motos[i].setModelo(modelo);
				motos[i].setCarenado(carenado);
				break;
			}
		}
	}
/**
 * cambia un modelo por otro
 * @param matricula
 * @param nuevoModelo
 */
	public void cambiarModelo(String matricula, String nuevoModelo) {
		for (int i = 0; i < motos.length; i++) {
			if (motos[i] != null) {
				if (motos[i].getMatricula().equals(matricula)) {
					motos[i].setCarenado(nuevoModelo);
				}
			}
		}
	}



	@Override
	public String toString() {
		return "ConcesionarioMotos [motos=" + Arrays.toString(motos) + ", equipamiento=" + equipamiento + "]";
	}
	public Motos[] getMotos() {
		return motos;
	}
	public void setMotos(Motos[] motos) {
		this.motos = motos;
	}
	public String getEquipamiento() {
		return equipamiento;
	}
	public void setEquipamiento(String equipamiento) {
		this.equipamiento = equipamiento;
	}
	
	


	
	
}
