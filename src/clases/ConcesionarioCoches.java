package clases;

import java.util.Arrays;
/**
 * 
 * @author Mario
 *
 */
public class ConcesionarioCoches extends Concesionario {

	private Coches[] coches;
	private int garantia;

	
	/**
	 * atributos concesonario coches
	 * @param nombre
	 * @param direccion
	 * @param cantidadVehiculos
	 * @param numeroTrabajadores
	 * @param maxMarcas
	 * @param numeroTelefono
	 * @param maxCoches
	 * @param garantia
	 */
	public ConcesionarioCoches(String nombre, String direccion, int cantidadVehiculos, int numeroTrabajadores,
			int maxMarcas, String numeroTelefono, int maxCoches, int garantia) {
		super(nombre, direccion, cantidadVehiculos, numeroTrabajadores, maxMarcas, numeroTelefono);
		this.coches = new Coches[maxCoches];
		this.garantia = garantia;
		this.nombre = "MC Concesionario";
	}
	
	
	public ConcesionarioCoches() {
		super();
		this.coches = new Coches [0];
		this.garantia = 0;
	}
/**
 * atributos y dar de alta coche
 * @param matricula
 * @param modelo
 */
	
	public void altaCoche(String matricula, String modelo) {
		for (int i = 0; i < coches.length; i++) {
			if (coches[i] == null) {
				coches[i] = new Coches(matricula);
				coches[i].setModelo(modelo);
				break;
			}
		}
	}
/**
 * comprueba si existe un coche
 * @param matricula
 */
	public void comprobarExistenciaCoche (String matricula) {
		int contador = 0;
		for (int i = 0; i < coches.length; i++) {
			if (coches[i] != null) {
				if (coches[i].getMatricula().equals(matricula)) {
					System.out.println("Si que existe");
					contador ++;
				}

			}
		}
		
		if (contador == 0) {
			System.out.println("No existe");
		}
		
	}

	
	@Override
	public String toString() {
		return "ConcesionarioCoches [coches=" + Arrays.toString(coches) + ", garantia=" + garantia + "]";
	}
	public Coches[] getCoches() {
		return coches;
	}
	public void setCoches(Coches[] coches) {
		this.coches = coches;
	}
	public int getGarantia() {
		return garantia;
	}
	public void setGarantia(int garantia) {
		this.garantia = garantia;
	}
	
	

	
	
}
