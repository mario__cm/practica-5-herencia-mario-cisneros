package programa;


import java.util.Scanner;

import clases.Concesionario;
import clases.ConcesionarioCoches;
import clases.ConcesionarioMotos;
/**
 * 
 * @author Mario
 *
 */
public class Programa {
	static Scanner in = new Scanner(System.in);

	public static void main(String[] args) {
		// TODO Auto-generated method stub
/**
 * menu principal
 */
		System.out.println("CONCESIONARIOS");
		int opcion;
		
		do {
			
			System.out.println("Menu");
			System.out.println("1. Crear concesionario");
			System.out.println("2. Crear concesionario de motos");
			System.out.println("3. Crear concesionario de coches");
			System.out.println("4. Salir");
			System.out.println("Introduce la opcion");

			opcion = in.nextInt();

			switch (opcion) {
			case 1:
				System.out.println("Has elegido crear concesionario");
				menuConcesionario();
				break;
				
			case 2:
				
				System.out.println("Has elegido crear concesionario de motos");
				menuConcesionarioMotos();
				break;
				
			case 3:
				System.out.println("Has elegido crear concesionario de coches");
				menuConcesionarioCoches();
				break;
			
			case 4:
				System.out.println("Muchas gracias por utilizar el programa");
				break;
			default:
				System.out.println("No has introducido una opcion valida");
				break;
			}
			
		} while (opcion != 4);
	}
	
	/**
	 * menu de concesionario padre
	 */
	public static void menuConcesionario() {

		Concesionario concesionario = new Concesionario("MC Concesionario", "Calle Logro�o", 30, 3, 5,
				"976234587");
		concesionario.nuevaMarca("A1", "Opel");
		concesionario.nuevaMarca("A2", "Audi");
		concesionario.nuevaMarca("A3", "Fiat");
		concesionario.nuevaMarca("A4", "Hyundai");
		concesionario.nuevaMarca("A5", "Jeep");
		concesionario.listarMarcas();
		
		System.out.println("Concesionario creado");
		int op;
				do {
					System.out.println("MENU");
					System.out.println("1. Mostrar atributos del concesionario");
					System.out.println("2. Utilizar metodo concesionario");
					System.out.println("3. Salir");
					op = in.nextInt();
					switch (op) {
					case 1:
						System.out.println("Mostrar atributos");
						System.out.println(concesionario.toString());
						break;
					case 2:
						System.out.println("Listar marcas");
						concesionario.listarMarcas();
						break;
					case 3:
						System.out.println("Gracias");
						break;
					default:
						System.out.println("Opcion incorrecta. Prueba otra vez.");
						break;
					}

				} while (op != 3);

		
	}
	/**
	 * menu de concesionarion hijo motos
	 */
	public static void menuConcesionarioMotos() {
		ConcesionarioMotos concesionarioM = new ConcesionarioMotos("Concesionario MC Motos", "Calle Viva Espa�a", 50, 30,
				5, "976548796", 5, "Cascos");
		
		concesionarioM.altaMoto("7485 HCT", "ER6N", "Naked");
		concesionarioM.altaMoto("5468 AHD", "CBR", "R");
		concesionarioM.altaMoto("4675 AFJ", "Z800", "Naked");
		concesionarioM.altaMoto("5445 IUH", "R6", "R");
		concesionarioM.altaMoto("5548 UHG", "FZ1", "SEMI-R");
		
		concesionarioM.nuevaMarca("A1", "Opel");
		concesionarioM.nuevaMarca("A2", "Audi");
		concesionarioM.nuevaMarca("A3", "Fiat");
		concesionarioM.nuevaMarca("A4", "Hyundai");
		concesionarioM.nuevaMarca("A5", "Jeep");
		
		int op1;
		System.out.println("Concesionario creado");
		
				do {
					System.out.println("MENU");
					System.out.println("1. Mostrar atributos del concesionario de motos");
					System.out.println("2. Utilizar metodo concesionario de motos");
					System.out.println("3. Utilizar metodo (padre) listar marcas");
					System.out.println("4. Salir");
					op1 = in.nextInt();
					switch (op1) {
					case 1:
						System.out.println("Mostrar atributos");
						System.out.println(concesionarioM.toString());
						break;
					case 2:
						System.out.println("Cambiar carenado");
						in.nextLine();
						System.out.println("Introduce matricula");
						String matricula = in.nextLine();
						System.out.println("Introduce nuevo modelo");
						String nuevoModelo = in.nextLine();
						concesionarioM.cambiarModelo(matricula, nuevoModelo);
						break;
					case 3:
						System.out.println("Listar marcas");
						concesionarioM.listarMarcas();
						break;
					case 4:
						System.out.println("Gracias");
						break;
					default:
						System.out.println("Opcion incorrecta. Prueba otra vez.");
						break;
					}

				} while (op1 != 4);
		
	}
	/**
	 * menu de concesionario hijo motos
	 */
	public static void menuConcesionarioCoches() {
		ConcesionarioCoches concesionarioC = new ConcesionarioCoches("Concesionario MC Coches", "Calle Radiador", 50, 30,
				5, "976554544", 5, 4);
		
		concesionarioC.altaCoche("6897 AGJ", "CIVIC");
		concesionarioC.altaCoche("5468 HGU", "A5");
		concesionarioC.altaCoche("6548 GDS", "420");
		concesionarioC.altaCoche("6596 AGJ", "LEON");
		concesionarioC.altaCoche("5487 HDF", "FABIA");
		
		concesionarioC.nuevaMarca("A1", "Opel");
		concesionarioC.nuevaMarca("A2", "Audi");
		concesionarioC.nuevaMarca("A3", "Fiat");
		concesionarioC.nuevaMarca("A4", "Hyundai");
		concesionarioC.nuevaMarca("A5", "Jeep");
		
		System.out.println("Concesionario creado");
		
		int op2;
		
		do {
			System.out.println("MENU");
			System.out.println("1. Mostrar atributos del concesionario de coches");
			System.out.println("2. Utilizar metodo concesionario de coches");
			System.out.println("3. Utilizar metodo (padre) listar marcas");
			System.out.println("4. Salir");
			op2 = in.nextInt();
			switch (op2) {
			case 1:
				System.out.println("Mostrar atributos");
				System.out.println(concesionarioC.toString());
				break;
			case 2:
				System.out.println("Comprobar existencia coche");
				in.nextLine();
				System.out.println("Introduce matricula");
				String matricula = in.nextLine();
				concesionarioC.comprobarExistenciaCoche(matricula);
				break;
			case 3:
				System.out.println("Listar marcas");
				concesionarioC.listarMarcas();
				break;
			case 4:
				System.out.println("Gracias");
				break;
			default:
				System.out.println("Opcion incorrecta. Prueba otra vez.");
				break;
			}

		} while (op2 != 4);
	
	}

}
